# [⚡️ Simple Dashboard ⚡️]
<p>&nbsp;</p>

[
<img alt="Horizon UI" src="https://gitlab.com/miclee0312/first-project/-/raw/main/assets/preview.png" /> ](https://gitlab.com/miclee0312/first-project)

<p>&nbsp;</p>

### 🎉Introduction

###### Why ReactJS?
One of the main benefits of using React JS is its potential to reuse components. It saves time for developers as they don't have to write various codes for the same features. Furthermore, if any changes are made in any particular part, it will not affect other parts of the application.
Read more about it here : [ReactJS](https://reactjs.org/)

##### Why React-Grid-Layout?
The grid system helps align page elements based on sequenced columns and rows. We use this column-based structure to place text, images, and functions in a consistent way throughout the design. Every element has its place that we can see instantly and reproduce elsewhere.
Read more about it here : [React-Grid-Layout](https://github.com/react-grid-layout/react-grid-layout)

##### Why React-Bootstrp?
Each component has been built from scratch as a true React component, without unneeded dependencies like jQuery. As one of the oldest React libraries, React-Bootstrap has evolved and grown alongside React, making it an excellent choice as your UI foundation.
Read more about it here : [React-Bootstrap](https://react-bootstrap.github.io/)

##### Why ApexChars.js?
ApexCharts is a modern charting library that I have ever experienced much to draw some 2D charts. This helps helps developers to create beautiful and interactive visualizations for web pages. It is an open-source project licensed under MIT and is free to use in commercial applications.
Read more about it here : [Apexcharts](https://apexcharts.com/) 

##### Why Plotly?
Plotly is an open-source Python graphing library that is great for building beautiful and interactive visualizations. Especially, I used to draw a lot of 3D graphs by using this library. The main benefits of Plotly are ease of use, advanced analytics, reduced costs, scalability, and total customization. Plotly is a user-friendly data visualization software that offers highly advanced visualization tools. No advanced training and knowledge are necessary to fully utilize all tools and features.
Read more about it here : [Plotly.JS](https://plotly.com/)


### 🎉Quick Start

Install Horizon UI by running either of the following:

- Install NodeJS LTS from
  [NodeJs Official Page](https://nodejs.org/en/?ref=horizon-documentation)
  (NOTE: Product only works with LTS version)

Clone the repository with the following command:

```bash
git clone https://gitlab.com/miclee0312/first-project.git
```

Run in terminal this command:

```bash
npm install
```

Then run this command to start your local server

```bash
npm start
```

