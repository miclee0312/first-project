import React from "react";
import AnalogClock from './AnalogClock';
import Time from '../../context/Time';
import DigitalClock from './DigitalClock';
import './Clock.css';

function Clock() {
  return (
    <section className="clock container">
      <div className="clock-container grid">
        <div className="clock-content grid">
          <Time>
            <AnalogClock />
            <DigitalClock />
          </Time>
        </div>
      </div>
    </section>
  );
}

export default Clock;
