import React from 'react';
import Plot from 'react-plotly.js';

export default class PlotEx extends React.Component {
  state = {
    line1: {
      x: [],
      y: [],
      z: [],
      type: 'surface',
      colorscale: 'Portland',
    },
    layout: { 
      datarevision: 0,
    },
    revision: 0,
  }

  componentDidMount() {
    this.generateData();
    setTimeout(() => {
      setInterval(this.generateData, 1);
    }, 2400)
  } 

  calc = (x, y) => {
    if ((1 - Math.pow(x, 2) - Math.pow(y, 2)) < 0) return 0
    else return(Math.pow((1 - Math.pow(x, 2) - Math.pow(y, 2)), 1/2))
  }

  generateData = () => {
    const {line1, layout} = this.state;
    line1.x = [];
    line1.y = [];
    line1.z = [];
    const date = new Date();
    const time = date.getSeconds() % 10 + date.getMilliseconds() / 1000;
    var steps = 10;
    var axisMax = 1;
    var axisStep = axisMax / steps;
    for (var x = 0; x < axisMax; x+=axisStep) {
      line1.x.push(x);
      let temp_values = [];
      for (var y = 0; y < axisMax; y+=axisStep) {
        line1.y.push(y);
        temp_values.push(time * this.calc(x, y));
      }
      line1.z.push(temp_values);
    }
    this.setState({ revision: this.state.revision + 1 });
    layout.datarevision = this.state.revision + 1;
  }

  render() {  
    return (<div>
      <Plot 
        data={[
          this.state.line1
        ]}
        layout={this.state.layout}
        revision={this.state.revision}
        graphDiv="graph"
      />
    </div>);
  }
}