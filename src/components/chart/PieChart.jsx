import ReactApexChart from 'react-apexcharts';
import React from 'react';

export default class ApexChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {      
            series: [25, 25, 44, 55, 41, 37, 32, 34, 26, 14],
            options: {
                chart: {
                    width: '100%',
                    type: 'pie',
                    height: '100%',
					animations: {
						enabled: true,
						easing: 'easeinout',
						speed: 1200
					}
                },
                labels: ["0-10", "10-20", "20-30", "30-40", "40-50", "50-60", "60-70", "70-80", "80-90", "90+"],
                theme: {
                    monochrome: {
                        enabled: true
                    }
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            offset: -5
                        }
                    }
                },
                title: {
                    text: "Population Distribution"
                },
                legend: {
                    show: false
                }
            },
        };
    }
    render() {
        return (
            <div id="chart">
            <ReactApexChart options={this.state.options} series={this.state.series} type="pie" />
            </div>
        );
    }
}