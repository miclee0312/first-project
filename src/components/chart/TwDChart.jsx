import { useState, useEffect, useRef } from 'react'
import ReactApexChart from 'react-apexcharts'
import React from "react";
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

const ChartBody = () => {
  const [tFlag, setTFlag] = useState(0);
  const [nFlag, setNFlag] = useState(0);
  var timer = useRef(0);
  const chartData = useRef([]);
  const options = {
    chart: {
      type: 'bar',
      toolbar: {
        show: false
      },
      animations: {
        enabled: false,
      },
      
    },
    xaxis: {
      position: 'top',
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      },
      crosshairs: {
        fill: {
          type: 'gradient',
          gradient: {
            colorFrom: '#D8E3F0',
            colorTo: '#BED1E6',
            stops: [0, 100],
            opacityFrom: 0.4,
            opacityTo: 0.5,
          }
        }
      },
      tooltip: {
        enabled: true,
      }
    },
    yaxis: {
      axisBorder: {
        show: true
      },
      axisTicks: {
        show: true,
      },
    },
    colors: [function({ value, seriesIndex, dataPointIndex, w }) {
      if (dataPointIndex % 5 === 4) {
        return '#7E36AF'
      } else {
        return '#D9534F'
      }
    }],
  }

  useEffect(() => {
    if (timer.current == 0) {
      getData(10);
    }
  }, []);

  const calc = (x_arr) => {
    var chart_arr = [];
    var today = new Date();
    var time = today.getSeconds() % 10;
    setTFlag(time);
    var y_arr = x_arr.map((single) => {
      single = single * (time);
      return single;
    });
    for (let i = 0; i < x_arr.length; i++) {
      chart_arr.push({x: x_arr[i].toString(), y: y_arr[i]})
    }
    chartData.current = chart_arr;
    timer.current = setTimeout(calc, 1000, x_arr);
  }

  const getData = (size) => {
    document.getElementById("size").value = size;
    var x_arr = [];
    for (let i = 1; i <= size; i++) {
      x_arr.push(i);
    }
    timer.current = setTimeout(calc, 1000, x_arr);
  }

  const onSubmit = () => {
    clearInterval(timer.current);
    setNFlag(timer.current);
    chartData.current = [];
    const size = document.getElementById("size").value;
    getData(size);
  }
  
  return (
    <div>
      <Container  className='mt-2' fluid="md">
        <Row className="justify-content-md-center">
          <Col xs="auto">
            <Form.Control type="number" className="mb-2" id="size"/>
          </Col>
          <Col xs="auto">
            <Button className="mb-2" onClick={onSubmit}>
              Submit
            </Button>
          </Col>
        </Row>
        <Row>
          <ReactApexChart options={options} series={[{data: chartData.current}]} type="bar" width={'800px'} height={'100%'}/>
        </Row>
      </Container>      
    </div>
  );
}

export default ChartBody;