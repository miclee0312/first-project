import React from "react";
import Clock from './components/analogclock/Clock';
import PieChart from './components/chart/PieChart';
import TwDChart from './components/chart/TwDChart';
import ThDChart from './components/chart/ThDChart';
import './App.css';

function App() {
  return (
    <div className="app_container">
      <aside><PieChart /></aside>
      <section><ThDChart /></section>
      <aside><Clock /></aside>
      <footer><TwDChart /></footer>
    </div>
  );
}

export default App;
